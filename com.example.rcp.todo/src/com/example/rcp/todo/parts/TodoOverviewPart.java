package com.example.rcp.todo.parts;

import javax.annotation.PostConstruct;

import org.eclipse.swt.widgets.Composite;

import com.example.rcp.todo.model.ITodoService;

public class TodoOverviewPart {
//    @Inject
//    @Optional
//    private ITodoService todoService;
    
    @PostConstruct
    public void createControls(Composite parent, ITodoService todoService) {
        System.out.println(this.getClass().getSimpleName()
                + " @PostConstruct method called.");

//        ITodoService todoService = TodoServiceFactory.getInstance();
        System.out.println(
                "Number of Todo objects " + todoService.getTodos().size());
    }
}
