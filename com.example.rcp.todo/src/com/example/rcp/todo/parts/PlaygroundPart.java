package com.example.rcp.todo.parts;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.annotation.PostConstruct;

import org.eclipse.e4.ui.di.Focus;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.resource.LocalResourceManager;
import org.eclipse.jface.resource.ResourceManager;
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.example.bundleresourceloader.IBundleResourceLoader;

public class PlaygroundPart {
//	 private Text text;
	    private Browser browser;
	    private Text text_1;

	    @PostConstruct
	    public void createControls(Composite parent, IBundleResourceLoader loader) {
	        parent.setLayout(new GridLayout(2, false));
	        new Label(parent, SWT.NONE);
//	        Label label = new Label(parent, SWT.NONE);
//	        
//	        ResourceManager resourceManager = 
//	                new LocalResourceManager(JFaceResources.getResources(),
//	                        label);
//	        
//	        Image image = resourceManager.createImage(
//	                loader.getImageDescriptor(this.getClass(), "images/5538152.png"));
//	        label.setImage(image);
//	        new Label(parent, SWT.NONE);
	        
	        Button btnNewButton = new Button(parent, SWT.NONE);
	        btnNewButton.addSelectionListener(new SelectionAdapter() {
	            @Override
	            public void widgetSelected(SelectionEvent e) {
	                System.out.println("Button selected");
	            }
	        });
	        btnNewButton.setText("New Button");
	        new Label(parent, SWT.NONE);
	        
	        Button btnNewButton_1 = new Button(parent, SWT.NONE);
	        btnNewButton_1.setText("New Button");
	        
	        Label lblNewLabel = new Label(parent, SWT.NONE);
	        lblNewLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
	        lblNewLabel.setText("New Label");
	        
	        text_1 = new Text(parent, SWT.BORDER);
	        text_1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
	        
//	    	System.out.println(this.getClass().getSimpleName()
//	    		    + " @PostConstruct method called.");
//	    	
//	        parent.setLayout(new GridLayout(2, false));
//
//	        text = new Text(parent, SWT.BORDER);
//	        text.setMessage("Enter City");
//	        text.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
//
//	        Button button = new Button(parent, SWT.PUSH);
//	        button.setText("Search");
//	        button.addSelectionListener(new SelectionAdapter() {
//	            @Override
//	            public void widgetSelected(SelectionEvent e) {
//	                String city = text.getText();
//	                if (city.isEmpty()) {
//	                    return;
//	                }
//	                try {
//	                    // not supported at the moment by Google
//	                    // browser.setUrl("http://maps.google.com/maps?q="
//	                    // + URLEncoder.encode(city, "UTF-8")
//	                    // + "&output=embed");
//	                    browser.setUrl("https://www.google.com/maps/place/"
//	                            + URLEncoder.encode(city, "UTF-8")
//	                            + "/&output=embed");
//
//	                } catch (UnsupportedEncodingException e1) {
//	                    e1.printStackTrace();
//	                }
//	            }
//	        });
//
//	        browser = new Browser(parent, SWT.NONE);
//	        browser.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));

	    }

	    @Focus
	    public void onFocus() {
//	        text.setFocus();
	    }
}
