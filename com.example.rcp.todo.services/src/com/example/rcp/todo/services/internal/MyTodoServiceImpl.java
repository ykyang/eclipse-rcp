/*
 * Copyright 2018 Yi-Kun Yang.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.rcp.todo.services.internal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.example.rcp.todo.model.ITodoService;
import com.example.rcp.todo.model.Todo;

public class MyTodoServiceImpl implements ITodoService {

    private static int current = 1;
    private List<Todo> todos;

    public MyTodoServiceImpl() {
        todos = createInitialModel();
    }

    @Override
    public Todo getTodo(long id) {
        Todo todo = findById(id);

        if (todo != null) {
            return todo.copy();
        }
        return null;
    }

    @Override
    public synchronized boolean saveTodo(Todo newTodo) {
        Todo updateTodo = findById(newTodo.getId());
        if (updateTodo == null) {
            updateTodo = new Todo(current++);
            todos.add(updateTodo);
        }
        updateTodo.setSummary(newTodo.getSummary());
        updateTodo.setDescription(newTodo.getDescription());
        updateTodo.setDone(newTodo.isDone());
        updateTodo.setDueDate(newTodo.getDueDate());

        return true;
    }

    @Override
    public boolean deleteTodo(long id) {
        Todo deleteTodo = findById(id);

        if (deleteTodo != null) {
            todos.remove(deleteTodo);
            return true;
        }
        return false;
    }

    @Override
    public List<Todo> getTodos() {
        List<Todo> list = new ArrayList<Todo>();
        for (Todo todo : todos) {
            list.add(todo.copy());
        }
        return list;
    }

    private List<Todo> createInitialModel() {
        List<Todo> list = new ArrayList<Todo>();
        list.add(createTodo("Application model", "Flexible and extensible"));
        list.add(createTodo("DI", "@Inject as programming mode"));
        list.add(createTodo("OSGi", "Services"));
        list.add(createTodo("SWT", "Widgets"));
        list.add(createTodo("JFace", "Especially Viewers!"));
        list.add(createTodo("CSS Styling", "Style your application"));
        list.add(createTodo("Eclipse services", "Selection, model, Part"));
        list.add(createTodo("Renderer", "Different UI toolkit"));
        list.add(createTodo("Compatibility Layer", "Run Eclipse 3.x"));
        return list;
    }

    private Todo createTodo(String summary, String description) {
        return new Todo(current++, summary, description, false, new Date());
    }

    private Todo findById(long id) {
        for (Todo todo : todos) {
            if (id == todo.getId()) {
                return todo;
            }
        }
        return null;
    }
}
