/*
 * Copyright 2018 Yi-Kun Yang.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.allnix.learn;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

public class GridLayoutSwt {
    static public void main(String[] args) {
        Display display = new Display();
        Shell shell = new Shell(display);
        GridLayout layout = new GridLayout(2, false);

        shell.setLayout(layout);
        {
            Label label = new Label(shell, SWT.NONE);
            label.setText("A Label");
            Button button = new Button(shell, SWT.PUSH);
            button.setText("Press Me");
        }
        {
            Label label = new Label(shell, SWT.BORDER);
            label.setText("This is a label");
            GridData gd = new GridData(SWT.FILL, SWT.TOP, true, false, 2, 1);
            label.setLayoutData(gd);
        }
        { // separator
            Label label = new Label(shell, SWT.SEPARATOR | SWT.HORIZONTAL);
            GridData gd = new GridData(SWT.FILL, SWT.TOP, true, false);
            gd.horizontalSpan = 2;
            label.setLayoutData(gd);
        }

        { // right aligned button
            Button button = new Button(shell, SWT.PUSH);
            button.setText("New Button");
            GridData gd = new GridData(SWT.RIGHT, SWT.TOP, false, false, 2, 1);
            button.setLayoutData(gd);
        }
        { // Spinner
            Spinner spinner = new Spinner(shell, SWT.READ_ONLY);
            spinner.setMinimum(0);
            spinner.setMaximum(1000);
            spinner.setSelection(500);
            spinner.setIncrement(1);
            spinner.setPageIncrement(100);
            GridData gd = new GridData(SWT.FILL, SWT.FILL, true, false);
            spinner.setLayoutData(gd);
        }

        { // group
            Composite comp = new Composite(shell, SWT.BORDER);
            GridData gd = new GridData(SWT.FILL, SWT.FILL, true, false);
            gd.horizontalSpan = 2;
            comp.setLayoutData(gd);
            comp.setLayout(new GridLayout(1, false));
            Text test = new Text(comp, SWT.NONE);
            test.setText("Testing");
            gd = new GridData(SWT.FILL, SWT.FILL, true, false);
            test.setLayoutData(gd);

            Text test2 = new Text(comp, SWT.NONE);
            test2.setText("Another Test");
        }

        { // group
            Group group = new Group(shell, SWT.NONE);
            group.setText("This is my group");
            GridData gd = new GridData(SWT.FILL, SWT.FILL, true, false);
            gd.horizontalSpan = 2;
            group.setLayoutData(gd);
            group.setLayout(new RowLayout(SWT.VERTICAL));
            Text text = new Text(group, SWT.NONE);
            text.setText("Another Test");
        }

        shell.pack();
        shell.open();
        while (!shell.isDisposed()) {
            if (!display.readAndDispatch()) {
                display.sleep();
            }
        }
        display.dispose();
    }
}
